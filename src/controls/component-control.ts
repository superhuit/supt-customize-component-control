import { ImageField, SELECTOR as SELECTOR_IMAGE_FIELD } from '../fields/image/image';
import { ColorField, SELECTOR as SELECTOR_COLOR_FIELD } from '../fields/color/color';
import { SelectTypoField, SELECTOR as SELECTOR_SELECT_TYPO_FIELD } from '../fields/select-typography/select-typography';
import { SelectColorField, SELECTOR as SELECTOR_SELECT_COLOR_FIELD } from '../fields/select-color/select-color';

export const SELECTOR  = '.supt-customize-component-control'
const SELECTOR_HANDLE  = `${SELECTOR}__head`
const SELECTOR_CONTENT = `${SELECTOR}__content`
const SELECTOR_INNER   = `${SELECTOR}__inner`
const SELECTOR_INPUTS  = `${SELECTOR}__inner input, ${SELECTOR}__inner select, ${SELECTOR}__inner textarea`;
const SELECTOR_FIELDS = `${SELECTOR}__field`;

interface HTMLFormElementAlt extends HTMLFormElement {
	addDelegateListener(eventType: string, selector: string, handler: Function, useCapture?: boolean): void
}

export interface RefsType {
	handle: HTMLElement,
	content: HTMLElement,
	inner: HTMLFormElementAlt,
	input: HTMLInputElement,
	parent: HTMLElement
}

interface StateType {
	isHidden: Boolean,
	value: Object,
}

interface PropsType {
	fieldsUnit: {[key: string]: string},
}

export class CustomizerComponentControl {
	el
	isCustom
	state: StateType
	props: PropsType
	refs: RefsType

	/**
	 * @param el The root dom element of the control
	 * @param isCustom Set this to true if you don't want to save default values to the database the first time the admin clicks "publish". Read below for more details.
	 *
	 * NOTE on isCustom:
	 * On any non-custom component, a 'change' event is triggered on load, in order to consider the input "dirty" and save it even if it wasn't changed. We do so in order to save its default value to the database on the first save.
	 * Set this attribute `isCustom` to true if you don't want this behavior.
	 * I.e. this is used on colors/fonts settings, where we have:
	 * - default fields that are always there (so isCustom === false because we want to add them to the database on first save, see `ColorField` or `SelectCustomTypoField`)
	 * - optional fields that we can "add" or "delete" (so isCustom === true because these control are hidden unless they exist in the database, see `CustomizerCustomColorControl` or `CustomizerCustomTypographyControl`)
	 */
	constructor(el: HTMLElement, isCustom: boolean) {
		this.el = el
		this.isCustom = isCustom;

		this.refs = {
			handle: this.el.querySelector(SELECTOR_HANDLE),
			content: this.el.querySelector(SELECTOR_CONTENT),
			inner: this.el.querySelector(SELECTOR_INNER),
			input: this.el.querySelector('[data-customize-setting-link]'),
			parent: this.el.closest('.accordion-section'),
		}

		let value;
		try { value = JSON.parse(this.refs.input.value) }
		catch { value = {} }

		this.state = {
			isHidden: true,
			value,
		}

		this.onSubInputChange = this.onSubInputChange.bind(this);
		this.refs.inner.addDelegateListener('change', 'input,select,textarea', this.onSubInputChange)

		if ( this.refs.handle ) {
			this.refs.handle.addEventListener('click', this.onHandleClick.bind(this) )
			this.refs.parent.addEventListener('component-open', this.onComponentOpen.bind(this))

			this.setContentHeight()
		}

		this.props = {
			fieldsUnit: this.getFieldsUnit()
		}

		// Trigger change if input has no value to save default values to DB
		if(!this.refs.input.value && !this.isCustom) {
			this.onSubInputChange();
		}

		this.getFields();
	}

	getFields() {
		const fields:Array<HTMLElement> = Array.from(this.el.querySelectorAll(SELECTOR_FIELDS));

		fields.map(field => {
			if(field.classList.contains(SELECTOR_IMAGE_FIELD)) {
				// Create image field
				new ImageField(field, {onChange: this.onSubInputChange});
			}
			else if(field.classList.contains(SELECTOR_COLOR_FIELD)) {
				// Create color field
				new ColorField(field);
			}
			else if(field.classList.contains(SELECTOR_SELECT_TYPO_FIELD)) {
				// Create select typography field
				new SelectTypoField(field);
			}
			else if(field.classList.contains(SELECTOR_SELECT_COLOR_FIELD)) {
				// Create select color field
				new SelectColorField(field, {onChange: this.onSubInputChange});
			}
		});
	}

	getFieldsUnit() {
		const inputs = Array.from(this.el.querySelectorAll(SELECTOR_INPUTS));

		const fields = inputs.reduce<{[key: string]: string}>((acc, input: HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement) => {
			acc[input.name] = input.dataset.unit;

			return acc;
		}, {});

		return fields;
	}

	onHandleClick(event: Event) {
		event.preventDefault()

		this.state.isHidden = !this.state.isHidden
		this.updateDOM()

		if ( !this.state.isHidden )
			this.refs.parent.dispatchEvent(new CustomEvent('component-open', { detail: { id: this.el.id }}))
	}

	onSubInputChange() {
		this.refs.input.value = this.getFormInputJSON(this.refs.inner);
		this.refs.input.dispatchEvent(new Event('change'));
	}

	onComponentOpen(event: CustomEvent) {
		if ( event.detail.id !== this.el.id ) {
			this.state.isHidden = true
			this.updateDOM();
		}
	}

	updateDOM() {
		this.el.classList.toggle( 'is-open', !this.state.isHidden )
		this.refs.content.setAttribute( 'aria-hidden', this.state.isHidden.toString() )
	}

	setContentHeight() {
		this.refs.content.style.height = `${this.refs.inner.offsetHeight}px`
	}

	getFormInputJSON(form: HTMLFormElement): string {
		const data: FormData = new FormData(form);

		let object: any = {};
		data.forEach((value, key) => {
			if(this.props.fieldsUnit[key] && value) {
				object[key] = value + this.props.fieldsUnit[key];
			}
			// if color empty -> saves color as transparent
			else if(key.endsWith('-c') || key.startsWith('--c-')) {
				object[key] = value==="" ? 'transparent' : value;
			}
			// if image. make sure image is saved with single quotes
			// 1. double quotes creates problems with doubles quotes used on final json
			// 2. no quotes won't work inside css
			else if (key.endsWith('-img')) {
				let finalValue = value;
				// @ts-ignore
				// add url() around img url for background-image css property to work
				if(value.charAt(0) !== 'u') {
					finalValue = `url(${value})`;
				}

				object[key] = finalValue;
			}
			else {
				object[key] = value;
			}
		});

		return JSON.stringify(object);
	}

	destroy() {

	}
}
