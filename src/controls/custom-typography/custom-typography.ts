import { CustomizerComponentControl, RefsType } from "../component-control";

export const SELECTOR = '.supt-customize-control-component-custom-typography';
export const SELECTOR_ADD_TYPO_BUTTON = '.supt-add-custom-typography-button';
const SELECTOR_CONTROL_HEAD = '.supt-customize-component-control__head-title';
const SELECTOR_NAME_INPUT = '.field-typography__custom-name__input';
const SELECTOR_DELETE_BUTTON = '.field-typography__delete-btn';

interface ButtonRefsType {
	addedItems: Array<HTMLElement>,
	hiddenItems: Array<HTMLElement>,
}
interface ButtonStateType {
	addedItemsCount: number
}
interface ButtonPropsType {
	maxItems: number
}

interface CustomTypographyRefsType extends RefsType {
	nameInput: HTMLInputElement,
	deleteButton: HTMLButtonElement,
	controlHead: HTMLElement
}

/**
 * Custom typography component control
 */
export class CustomizerCustomTypographyControl extends CustomizerComponentControl {
	refs: CustomTypographyRefsType

	constructor(el: HTMLElement) {
		super(el, true);

		// bindings
		this.handleNameInputChange = this.handleNameInputChange.bind(this);
		this.handleDeleteButtonClick = this.handleDeleteButtonClick.bind(this);

		// refs
		this.refs.nameInput = this.el.querySelector(SELECTOR_NAME_INPUT);
		this.refs.deleteButton = this.el.querySelector(SELECTOR_DELETE_BUTTON);
		this.refs.controlHead = this.el.querySelector(SELECTOR_CONTROL_HEAD);

		// events
		this.refs.nameInput.addEventListener("change", this.handleNameInputChange);
		this.refs.deleteButton.addEventListener("click", this.handleDeleteButtonClick);

		// init
		if(this.refs.input.value) {
			// Update typo name with custom name if defined
			this.refs.controlHead.innerHTML = this.refs.nameInput.value;
		}
		else {
			// Hide custom typography styles if no value / haven't been added
			this.el.classList.add("is-hidden");
		}
	}

	handleNameInputChange(event: Event) {
		const target = event.target as HTMLInputElement;

		this.refs.controlHead.innerHTML = target.value;

		document.dispatchEvent(new CustomEvent('custom-typography-updated', { detail: { item: this.el }}));
	}

	handleDeleteButtonClick(event: Event) {
		event.preventDefault();

		document.dispatchEvent(new CustomEvent('custom-typography-deleted', { detail: { item: this.el }}));
		this.el.classList.add("is-hidden");
		this.refs.input.value = "";
		this.refs.input.dispatchEvent(new Event('change')); // Trigger change to enable publish button
	}
}

/**
 * Button to add custom typography control
 */
export class CustomizerButtonAddTypoControl {
	el
	refs: ButtonRefsType
	state: ButtonStateType
	props: ButtonPropsType

	constructor(el: HTMLButtonElement) {
		this.handleButtonClick = this.handleButtonClick.bind(this);
		this.onItemDeleted = this.onItemDeleted.bind(this);

		this.el = el;

		this.refs = {
			addedItems: Array.from(document.querySelectorAll(`${SELECTOR}:not(.is-hidden)`)),
			hiddenItems: Array.from(document.querySelectorAll(`${SELECTOR}.is-hidden`)),
		}

		this.state = {
			addedItemsCount: this.refs.addedItems.length
		};

		this.props = {
			maxItems: this.refs.addedItems.length + this.refs.hiddenItems.length
		};

		// event listeners
		this.el.addEventListener("click", this.handleButtonClick);
		document.addEventListener('custom-typography-deleted', this.onItemDeleted);
	}


	handleButtonClick(event: Event) {
		event.preventDefault();

		if(this.state.addedItemsCount < this.props.maxItems) {
			this.state.addedItemsCount += 1;

			this.addItem();

			if(this.state.addedItemsCount === this.props.maxItems) {
				this.el.setAttribute("disabled", "true");
			}
		}
	}

	addItem() {
		const newItem = this.refs.hiddenItems[0];
		newItem.classList.remove('is-hidden');
		this.refs.addedItems.push(newItem);
		this.refs.hiddenItems.shift();

		document.dispatchEvent(new CustomEvent('custom-typography-added', { detail: { newItem }}))
	}

	onItemDeleted(event: CustomEvent) {
		this.refs.addedItems.forEach((item, index) => {
			if(item.id === event.detail.item.id) {
				this.refs.addedItems.splice(index, 1);
				this.refs.hiddenItems.unshift(item);
			}
		})
	}
}
