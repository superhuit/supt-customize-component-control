import { CustomizerComponentControl, RefsType } from "../component-control";

export const SELECTOR = '.supt-customize-control-component-custom-theme';
const SELECTOR_CONTROL_HEAD = '.supt-customize-component-control__head-title';
const SELECTOR_NAME_INPUT = '.field-theme__custom-name__input';
const SELECTOR_DELETE_BUTTON = '.field-theme__delete-btn';


interface CustomThemeRefsType extends RefsType {
	nameInput: HTMLInputElement,
	deleteButton: HTMLButtonElement,
	controlHead: HTMLElement
}

/**
 * Custom themes component control
 */
export class CustomizerCustomThemeControl extends CustomizerComponentControl {
	refs: CustomThemeRefsType

	constructor(el: HTMLElement) {
		super(el, true);

		// bindings
		this.handleNameInputChange = this.handleNameInputChange.bind(this);
		this.handleDeleteButtonClick = this.handleDeleteButtonClick.bind(this);

		// refs
		this.refs.nameInput = this.el.querySelector(SELECTOR_NAME_INPUT);
		this.refs.deleteButton = this.el.querySelector(SELECTOR_DELETE_BUTTON);
		this.refs.controlHead = this.el.querySelector(SELECTOR_CONTROL_HEAD);


		// // events
		this.refs.nameInput.addEventListener("change", this.handleNameInputChange);
		this.refs.deleteButton.addEventListener("click", this.handleDeleteButtonClick);

		// init
		if(this.refs.input.value) {
			// Update theme name with custom name if defined
			this.refs.controlHead.innerHTML = this.refs.nameInput.value;
		}
		else {
			// Hide custom theme if no value / haven't been added
			this.el.classList.add("is-hidden");
		}
	}

	handleNameInputChange(event: Event) {
		const target = event.target as HTMLInputElement;

		this.refs.controlHead.innerHTML = target.value;
	}

	handleDeleteButtonClick(event: Event) {
		event.preventDefault();

		document.dispatchEvent(new CustomEvent('custom-theme-deleted', { detail: { item: this.el }}));
		this.el.classList.add("is-hidden");
		this.refs.input.value = "";
		this.refs.input.dispatchEvent(new Event('change')); // Trigger change to enable publish button
	}
}
