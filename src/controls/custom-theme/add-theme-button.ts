import { SELECTOR } from './custom-theme';

interface ButtonRefsType {
	addedItems: Array<HTMLElement>,
	hiddenItems: Array<HTMLElement>,
}
interface ButtonStateType {
	addedItemsCount: number
}
interface ButtonPropsType {
	maxItems: number
}

export const SELECTOR_ADD_THEME_BUTTON = '.supt-add-theme-button';

/**
 * Button to add custom theme control
 */
export class CustomizerAddThemeButtonControl {
	el
	refs: ButtonRefsType
	state: ButtonStateType
	props: ButtonPropsType

	constructor(el: HTMLButtonElement) {
		this.handleButtonClick = this.handleButtonClick.bind(this);
		this.onItemDeleted = this.onItemDeleted.bind(this);

		this.el = el;

		this.refs = {
			addedItems: Array.from(document.querySelectorAll(`${SELECTOR}:not(.is-hidden)`)),
			hiddenItems: Array.from(document.querySelectorAll(`${SELECTOR}.is-hidden`)),
		}

		this.state = {
			addedItemsCount: this.refs.addedItems.length
		};

		this.props = {
			maxItems: this.refs.addedItems.length + this.refs.hiddenItems.length
		};

		// event listeners
		this.el.addEventListener("click", this.handleButtonClick);
		document.addEventListener('custom-theme-deleted', this.onItemDeleted);
	}


	handleButtonClick(event: Event) {
		event.preventDefault();

		if(this.state.addedItemsCount < this.props.maxItems) {
			this.state.addedItemsCount += 1;

			this.addItem();

			if(this.state.addedItemsCount === this.props.maxItems) {
				this.el.setAttribute("disabled", "true");
			}
		}
	}

	addItem() {
		const newItem = this.refs.hiddenItems[0];
		newItem.classList.remove('is-hidden');
		this.refs.addedItems.push(newItem);
		this.refs.hiddenItems.shift();
	}

	onItemDeleted(event: CustomEvent) {
		this.refs.addedItems.forEach((item, index) => {
			if(item.id === event.detail.item.id) {
				this.refs.addedItems.splice(index, 1);
				this.refs.hiddenItems.unshift(item);
			}
		})
	}
}
