<?php

namespace SUPT\Customizer\Control\Fields\Dimensions;

function render_field($id, $name, $attrs, $echo = true) {
	$sub_fields = [];

	foreach ($attrs['subfields'] as $sub_name => $sub_attrs) {
		$units = ( !empty($sub_attrs['unit']) || !empty($attrs['unit']) ) ? (!empty($sub_attrs['unit']) ? $sub_attrs['unit'] : $attrs['unit']) : null;

		// save unit as part of the value - default: true
		$save_unit = ( !empty($sub_attrs['save_unit']) || !empty($attrs['save_unit']) ) ? (!empty($sub_attrs['save_unit']) ? $sub_attrs['save_unit'] : $attrs['save_unit']) : 'true';

		$value = $attrs['value'][$name.'-'.$sub_name] ?? null;

		$sub_data = [
			'%field_id'    => "{$id}_{$name}_{$sub_name}_field",
			'%id'          => "{$id}_{$name}_{$sub_name}",
			'%label'       => $sub_attrs['label'] ?? $sub_name,
			'%type'        => $sub_attrs['type'] ?? 'text',
			'%name'        => "{$name}-$sub_name",
			'%unit'        => ( (empty($units) || $save_unit === 'false') ? '' : " data-unit=\"$units\"" ),
			'%show_unit'   => ( empty($units) ? '' : " data-unit=\"$units\"" ),
			'%placeholder' => $sub_attrs['placeholder'] ?? '',
			'%value'       => $value ? str_replace($units, '', $value) : ($sub_attrs['default'] ?? ''),
			'%attrs'       => (empty($sub_attrs['attrs']) ? '' :	' '.implode( ' ',array_map(
				function($k) use ($sub_attrs) {
					return "{$k}=\"{$sub_attrs['attrs'][$k]}\"";
				},
				array_keys($sub_attrs['attrs'])
			))),
		];
		$sub_fields[] = str_replace(
			array_keys($sub_data),
			array_values($sub_data),
			'<div class="dimensions-field__item" id="%field_id"%show_unit>
				<label for="%id">%label</label>
				<input type="%type" id="%id" name="%name" value="%value" placeholder="%placeholder"%attrs %unit/>
			</div>'
		);
	}

	$data = [
		'%label'      => $attrs['label'],
		'%sub_fields' => implode("\n", $sub_fields),
	];

	$html = str_replace(
		array_keys($data),
		array_values($data),
		'<fieldset class="supt-customize-component-control__field dimensions-field">
			<legend class="dimensions-field__legend">%label</legend>
			<div class="dimensions-field__inner">
				%sub_fields
			</div>
		</fieldset>'
	);

	if ($echo) echo $html;
	return $html;
}
