<?php

namespace SUPT\Customizer\Control\Fields;

require_once __DIR__.'/color/color.php';
require_once __DIR__.'/dimensions/dimensions.php';
require_once __DIR__.'/typography/typography.php';
require_once __DIR__.'/select-typography/select-typography.php';
require_once __DIR__.'/select-color/select-color.php';
require_once __DIR__.'/image/image.php';
require_once __DIR__.'/theme/theme.php';
