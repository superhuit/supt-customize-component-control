export const SELECTOR = 'field-color';
const INPUT_SELECTOR = 'input[data-type="color"]';

interface RefsType {
	input: HTMLInputElement,
	inputColor: HTMLInputElement,
}

export class ColorField {
	el
	refs: RefsType

	constructor(el: HTMLElement) {
		this.el = el;

		this.refs = {
			input: this.el.querySelector(INPUT_SELECTOR),
			inputColor: document.createElement("input"),
		}

		// if color transparent -> show empty input
		if(this.refs.input.value === "transparent") {
			this.refs.input.value = "";
		}
		// input color
		this.refs.inputColor.type = 'color';
		this.refs.inputColor.value = this.refs.input.value;
		this.refs.input.insertAdjacentElement("beforebegin", this.refs.inputColor);

		// event listeners
		this.refs.input.addEventListener("change", this.onChange.bind(this));
		this.refs.inputColor.addEventListener("change", this.onChange.bind(this));
	}


	onChange(event: Event) {
		const target = (event.target as HTMLInputElement);
		const inputType = target.type;
		const newValue = target.value;

		//update sibling input
		if(inputType === "color") {
			this.refs.input.value = newValue;
		}
		else if(inputType === "text") {
			this.refs.inputColor.value = newValue;
		}

		// Dispatch event to notify that the color has changed (for select-color)
		document.dispatchEvent(new CustomEvent('color-updated', { detail: { id: this.refs.input.name, value: newValue }}));
	}

	destroy() {

	}
}
