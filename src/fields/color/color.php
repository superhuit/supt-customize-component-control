<?php

namespace SUPT\Customizer\Control\Fields\Color;

class ColorControl {

	function __construct($id, $name, $attrs) {
		$this->id = $id;
		$this->name = $name;
		$this->attrs = $attrs;
	}

	function get_id() {
		return "{$this->id}_{$this->name}";
	}

	function get_rendered() {
		$data = [
			'%id'      => $this->get_id().'_color',
			'%name'		 => $this->name.'-color',
			'%label'   => $this->attrs['label'],
			'%type'   => $this->attrs['type'],
			'%value'   => $this->attrs['value'][$this->name.'-color'] ?? $this->attrs['default']['color'] ?? '#ffffff',
		];

		return str_replace(
			array_keys($data),
			array_values($data),
			'<div class="supt-customize-component-control__field field-color"'. (isset($this->attrs["is_custom"]) ? "data-supt-custom-color" : "") .'>
				<label class="supt-customize-component-control__field__label" for="%id">%label</label>'.
				(isset($this->attrs["is_custom"]) ? $this->get_custom_name_input() : '').
				'
				<div class="supt-customize-component-control__input-wrapper field-color__input-wrapper">
					<input class="field-color__input-text" data-type="%type" type="text" id="%id" name="%name" size="7" maxlength="7" pattern="#[0-9A-Fa-f]{6}" value="%value"/>
				</div>' .
				(isset($this->attrs["is_custom"]) ? '<button class="field-color__delete-btn button button-link button-link-delete">Delete</button>' : '').
			'
			</div>'
		);
	}

	function get_custom_name_input() {
		$data = [
			'%id' 	 => "{$this->get_id()}_name",
			'%name'  => "{$this->name}-name",
			'%label' => __('Color custom name', 'supt-ccc'),
			'%value' => $this->attrs['value'][$this->name.'-name'] ?? $this->attrs['label'] ?? '',
		];

		return str_replace(
			array_keys($data),
			array_values($data),
			'<div class="field-color__item field-color__custom-name --custom-name">
				<label for="%id" class="field-color__custom-name__label">%label</label>
				<input type="text" id="%id" class="field-color__custom-name__input" name="%name" value="%value" />
			</div>'
		);
	}
}

function render_field($id, $name, $attrs, $echo = true) {
	$colorControl = new ColorControl($id, $name, $attrs);
	$html = $colorControl->get_rendered();

	if ($echo) echo $html;
	return $html;
}
