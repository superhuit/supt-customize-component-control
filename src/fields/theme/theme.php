<?php

namespace SUPT\Customizer\Control\Fields\Theme;

use function SUPT\Customizer\Control\Fields\SelectColor\render_field as render_field_select_color;

class ThemeField {

	function __construct($id, $name, $attrs) {
		$this->id = $id;
		$this->name = $name;
		$this->attrs = $attrs;
	}

	function get_id() {
		return "{$this->id}_{$this->name}";
	}

	function get_rendered() {

		return '<div class="supt-customize-component-control__field field-theme data-supt-custom-theme">'.
			$this->get_custom_name_input().
			$this->get_background_color_input().
			$this->get_foreground_color_input().
			'<button class="field-theme__delete-btn button button-link button-link-delete">Delete</button>
		</div>';
	}

	function get_custom_name_input() {
		$data = [
			'%id' 	 => "{$this->get_id()}_name",
			'%name'  => "{$this->name}-name",
			'%label' => __('Theme name', 'supt-ccc'),
			'%value' => $this->attrs['value'][$this->name.'-name'] ?? $this->attrs['default']['name'] ?? '',
		];

		return str_replace(
			array_keys($data),
			array_values($data),
			'<div class="field-theme__item field-theme__custom-name --custom-name">
				<label for="%id" class="field-theme__custom-name__label">%label</label>
				<input type="text" id="%id" class="field-theme__custom-name__input" name="%name" value="%value" />
			</div>'
		);
	}

	function get_background_color_input() {
		$bc_attrs = [
			'id' 	 			 => "{$this->get_id()}_b_color",
			'name'  		 => "{$this->name}-b-color",
			'label' 		 => __('Background color', 'supt-ccc'),
			'value' 		 => $this->attrs['value'][$this->name.'-b-color'] ?? '',
			'default' 	 => $this->attrs['default']['b-color'] ?? '',
			'type' 			 => 'select_color',
			'color_type' => ''
		];

		$attrs = array_merge($this->attrs, $bc_attrs);
		return render_field_select_color($attrs['id'], $attrs['name'], $attrs, false);
	}

	function get_foreground_color_input() {
		$fc_attrs = [
			'id' 	 		=> "{$this->get_id()}_f_color",
			'name'  	=> "{$this->name}-f-color",
			'label' 	=> __('Foreground color', 'supt-ccc'),
			'value' 	=> $this->attrs['value'][$this->name.'-f-color'] ?? '',
			'default' => $this->attrs['default']['f-color'] ?? '',
			'type' 			 => 'select_color',
			'color_type' => ''
		];

		$attrs = array_merge($this->attrs, $fc_attrs);
		return render_field_select_color($attrs['id'], $attrs['name'], $attrs, false);
	}
}

function render_field($id, $name, $attrs, $echo = true) {
	$themeField = new ThemeField($id, $name, $attrs);
	$html = $themeField->get_rendered();

	if ($echo) echo $html;
	return $html;
}
