<?php

namespace SUPT\Customizer\Control\Fields\SelectColor;

function render_field($id, $name, $attrs, $echo = true) {
	$is_only_color = $attrs['type'] === 'select_color'; // Is not full colors (so only the color-select will be displayed)
	$is_only_gradient = $attrs['type'] === 'select_gradient'; // Is not full colors (so only the gradient-select will be displayed)

	$default_options = get_default_color_options($attrs['colors_setting_id'], $attrs['default_colors']);
	$custom_options = get_custom_color_options($attrs['colors_setting_id']);

	/**
	 * Get values type
	 */
	$value_is_inherit = $attrs['value'] ? str_starts_with($attrs['value'], "var(--theme") : false;
	$value_is_transparent = $attrs['value'] ? $attrs['value'] === "transparent" : false;
	$value_is_color = $is_only_color ? true : (($attrs['value'] && !$value_is_inherit && !$value_is_transparent) ? str_starts_with($attrs['value'], "var(") : false); // If it's only colors, it will always be the color select displayed
	$value_is_gradient = $is_only_gradient ? true : ($attrs['value'] ? (!$value_is_inherit && !$value_is_transparent && !$value_is_color) : false); // If it's only gradient, it will always be the gradient select displayed

	/**
	 * Get values
	 */
	// Get inherit value (from current variant theme and color type (background/foreground))
	$current_theme = explode('settings-variants', $id)[1] ?? ''; // get current variant theme
	$current_theme = str_replace('-hover', '', $current_theme); // remove '-hover' because it doesn't exist in inherit values
	if($attrs['color_type'] === 'background') {
		$inherit_value = 'var(--' . $current_theme . '-b-color)';
	}
	else { // if foreground or fallback if not defined
		$inherit_value = 'var(--' . $current_theme . '-f-color)';
	}

	// Get color value
	$color_value = $value_is_color ? ($attrs['value'] ?? $attrs['default'] ?? '') : array_keys($default_options)[0];

	// Get gradient value
	$gradient_value = $value_is_gradient ? ($attrs['value'] ?? $attrs['default'] ?? '') : "";

	/**
	 * Colors options
	 */
	$options = [];
	foreach (array_merge($default_options, $custom_options) as $v => $n) {
		// Fetch exact color value to display color next to the label in the select option
		$color_setting = str_replace(['var(', '-color)'], '', $v);
		$color = json_decode(get_theme_mod(sprintf($attrs['colors_setting_id'], $color_setting), "{}"), true);

		// Option HTML template
		$options[] = sprintf(
			'<option value="%1$s" data-color="%4$s" %3$s>%2$s</option>',
			$v,
			$n,
			( $v == $color_value ? ' selected' : ''),
			$color[$color_setting . '-color']
		);
	}

	$data = [
		'%id'             => $id."_".$name,
		'%name'           => $name,
		'%label'          => $attrs['label'],
		'%inherit_value'	=> $inherit_value,
		'%color_value' 	  => $color_value,
		'%gradient_value' => $gradient_value,
		'%options'        => implode("\n", $options),
	];

	$html = str_replace(
		array_keys($data),
		array_values($data),
		'<fieldset class="supt-customize-component-control__field select-color-field">
			<legend class="select-color-field__legend">%label</legend>
			<div class="select-color-field__inner">
				<div class="select-color-field__item">
					<select data-type="select-default" id="%id" name="%name" class="' . (($is_only_color || $is_only_gradient) ? 'is-hidden' : '') . '">
						<option value="%inherit_value" '. ($value_is_inherit ? "selected" : "") .'>Inherit</option>
						<option value="transparent" '. ($value_is_transparent ? "selected" : "") .'>Transparent</option>
						<option value="%color_value" data-toggle-target="select-color" '. ($value_is_color ? "selected" : "") .'>Color</option>'
						. (($attrs['color_type'] === "background" || $is_only_gradient) ? '<option value="%gradient_value" data-toggle-target="select-gradient" '. ($value_is_gradient ? "selected" : "") .'>Gradient</option>' : '') . // Show gradient for background color type or if only gradient
					'</select>

					<select class="select-color-field__custom-select optional' . ($value_is_color ? ' is-visible' : '') . '" data-type="select-color">%options</select>'

					// Show gradient for background color type or if only gradient
					. (($attrs['color_type'] === "background" || $is_only_gradient) ?
							'<div class="gradient optional' . ($value_is_gradient ? ' is-visible' : '') . '" data-type="select-gradient" data-value="%gradient_value">
								<div class="gradient__picker"></div>
								<div class="gradient__picker-inputs">
									<select class="form-control switch-type">
										<option value="">- Select Type -</option>
										<option value="radial">Radial</option>
										<option value="linear">Linear</option>
										<option value="repeating-radial">Repeating Radial</option>
										<option value="repeating-linear">Repeating Linear</option>
									</select>

									<select class="form-control switch-angle">
										<option value="">- Select Direction -</option>
										<option value="top">Top</option>
										<option value="right">Right</option>
										<option value="center">Center</option>
										<option value="bottom">Bottom</option>
										<option value="left">Left</option>
									</select>
								</div>
							</div>'
						: '') .
				'</div>
			</div>
		</fieldset>'
	);

	if ($echo) echo $html;
	return $html;
}

function get_default_color_options($setting_id, $default) {
	return array_reduce(array_keys($default), function($acc, $key) use ($setting_id, $default) {
		$settings = json_decode(get_theme_mod(sprintf($setting_id, $key), "{}"), true);

		if (!empty($settings)) $acc += ["var($key-color)" => $default[$key]['label']];

		return $acc;
	}, []);
}

function get_custom_color_options($setting_id) {
	$options = [];
	$custom_colors = array_fill(1, 20, '');

	return array_reduce(array_keys($custom_colors), function($acc, $key) use ($setting_id) {
		$settings = json_decode(get_theme_mod(sprintf($setting_id, '--c-custom-' . $key), "{}"), true);

		$option_name = $settings['--c-custom-' . $key . '-name'] ?? "Custom color " . $key;

		if (!empty($settings)) $acc += ['var(--c-custom-' . $key . '-color)' => $option_name];

		return $acc;
	}, []);
}
