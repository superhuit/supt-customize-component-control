const Grapick = require('grapick');
import "grapick/dist/grapick.min.css";

export const SELECTOR = 'select-color-field';
const DEFAULT_SELECT_SELECTOR = 'select[data-type="select-default"]';
const COLOR_SELECT_SELECTOR = '[data-type="select-color"]';
const GRADIENT_SELECT_SELECTOR = '[data-type="select-gradient"]';

const OPTION_COLOR_SELECTOR = 'select[data-type="select-default"] option[data-toggle-target="select-color"]';
const OPTION_GRADIENT_SELECTOR = 'select[data-type="select-default"] option[data-toggle-target="select-gradient"]';

const CUSTOM_COLOR_ID_PREFIX = "supt-customize-component-control-supt-mods-settings_colors-";
const SELECTOR_CONTROL_HEAD = '.supt-customize-component-control__field__label';

declare global {
	interface Window {
		jQuery:any;
	}
}

interface RefsType {
	defaultSelect: HTMLSelectElement,
	colorSelect: HTMLSelectElement,
	gradientSelect: HTMLSelectElement
	colorOptionEl: HTMLOptionElement,
	gradientOptionEl: HTMLOptionElement,
	colorOptions: Array<HTMLOptionElement>
	gradientPicker?: any; // Grapick type
}
interface PropsType {
	onChange: Function
}

export class SelectColorField {
	el
	refs: RefsType
	props: PropsType

	constructor(el: HTMLElement, props: PropsType) {
		this.onDefaultSelectUpdate = this.onDefaultSelectUpdate.bind(this);
		this.onCustomColorAdded = this.onCustomColorAdded.bind(this);
		this.onCustomColorDeleted = this.onCustomColorDeleted.bind(this);
		this.onCustomColorUpdated = this.onCustomColorUpdated.bind(this);
		this.onColorUpdated = this.onColorUpdated.bind(this);
		this.onColorSelectChange = this.onColorSelectChange.bind(this);
		this.onGradientSelectChange = this.onGradientSelectChange.bind(this);

		this.el = el;
		this.props = props;

		this.refs = {
			defaultSelect: this.el.querySelector(DEFAULT_SELECT_SELECTOR),
			colorSelect: this.el.querySelector(COLOR_SELECT_SELECTOR),
			gradientSelect: this.el.querySelector(GRADIENT_SELECT_SELECTOR),
			colorOptionEl: this.el.querySelector(OPTION_COLOR_SELECTOR),
			gradientOptionEl: this.el.querySelector(OPTION_GRADIENT_SELECTOR),
			colorOptions: Array.from(this.el.querySelectorAll(`${COLOR_SELECT_SELECTOR} option`)),
		}

		// Create custom gradient color picker
		if(this.refs.gradientSelect) {
			this.createGradientPicker();
		}

		// Create custom select using Select2 that works with jQuery (automatically imported in WP)
		window.jQuery(this.refs.colorSelect).select2({
			templateResult: (data: any) => {
				const color = data.element ? data.element.dataset.color : '';
				return window.jQuery(`<span><span class="color-icon" style="background-color: ${color}"></span>${data.text}</span>`);
			}
		}).on('change', this.onColorSelectChange);

		// event listeners
		document.addEventListener('custom-color-added', this.onCustomColorAdded);
		document.addEventListener('custom-color-deleted', this.onCustomColorDeleted);
		document.addEventListener('custom-color-updated', this.onCustomColorUpdated);
		document.addEventListener('color-updated', this.onColorUpdated);
		this.refs.defaultSelect.addEventListener('change', this.onDefaultSelectUpdate);
	}

	createGradientPicker() {
		this.refs.gradientPicker = new Grapick({
			el: this.refs.gradientSelect.querySelector(".gradient__picker"),
		});

		// Set value
		if(this.refs.gradientSelect.dataset.value)
			this.refs.gradientPicker.setValue(this.refs.gradientSelect.dataset.value);

		// Handle type and angle changes for the gradient
		const swType:HTMLSelectElement = this.refs.gradientSelect.querySelector('.switch-type');
		const swAngle:HTMLSelectElement = this.refs.gradientSelect.querySelector('.switch-angle');
		swType.addEventListener('change', (event: { target: any; }) => {
			this.refs.gradientPicker?.setType(event.target.value || 'linear');
		});
		swAngle.addEventListener('change', (event: { target: any; }) => {
			this.refs.gradientPicker?.setDirection(event.target.value || 'right');
		});

		// Set type + angle value
		swType.value = this.refs.gradientPicker.getType();
		swAngle.value = this.refs.gradientPicker.getDirection()?.replace("to ", "");

		// Handle change listener
		this.refs.gradientPicker.on('change', this.onGradientSelectChange);
	}

	onColorSelectChange(event: { target: any; }) {
		this.refs.colorOptionEl.value = event.target.value;

		this.props.onChange(); // Trigger manually onSubInputChange because Select2 is a <span> and not triggerring default form change events
	}
	onGradientSelectChange() {
		this.refs.gradientOptionEl.value = this.refs.gradientPicker.getValue();
		this.props.onChange(); // Trigger manually onSubInputChange because Grapick is a <div> and not triggerring default form change events
	}

	onControlContentHeight() {
		setTimeout(() => {
			let contentEl = this.el.closest('.supt-customize-component-control__content') as HTMLElement;
			let innerEl = this.el.closest('.supt-customize-component-control__inner') as HTMLElement;
			contentEl.style.height = `${innerEl.offsetHeight}px`;
		}, 0);
	}

	onDefaultSelectUpdate(event: { target: any; }) {
		this.onControlContentHeight();

		const target = event.target;
		const selectedIndex = target.selectedIndex;
		const toggleTarget = target.options[selectedIndex].getAttribute('data-toggle-target');

		if(toggleTarget) {
			switch(toggleTarget) {
				case "select-color":
					this.refs.colorSelect.classList.add("is-visible");
					this.refs.gradientSelect?.classList.remove("is-visible");
					break;
				case "select-gradient":
					this.refs.gradientSelect?.classList.add("is-visible");
					this.refs.colorSelect.classList.remove("is-visible");
					break;
			}
		}
		else {
			this.refs.gradientSelect?.classList.remove("is-visible");
			this.refs.colorSelect.classList.remove("is-visible");
		}
	}

	/**
	 * Handle custom color added/updated/deleted to update select-color values without refreshing page
	 */
	onCustomColorAdded(event: CustomEvent) {
		event.preventDefault();

		// Add new option in select with new color item added
		const newOption = document.createElement('option');
		newOption.setAttribute('value', 'var(' + event.detail.newItem.id.replace(CUSTOM_COLOR_ID_PREFIX, '') + '-color)');
		newOption.innerHTML = event.detail.newItem.querySelector(SELECTOR_CONTROL_HEAD).innerHTML;

		this.refs.colorSelect.appendChild(newOption);
		this.refs.colorOptions.push(newOption);
	}

	onCustomColorDeleted(event: CustomEvent) {
		// Remove deleted item/option in select
		this.refs.colorOptions.forEach((option, index) => {
			if(option.value === event.detail.item.id.replace(CUSTOM_COLOR_ID_PREFIX, '')) {
				this.refs.colorSelect.removeChild(option);
				this.refs.colorOptions.splice(index, 1);
			}
		});
	}

	onCustomColorUpdated(event: CustomEvent) {
		// Update updated item/option in select
		this.refs.colorOptions.forEach((option) => {
			if(option.value === ('var(' + event.detail.item.id.replace(CUSTOM_COLOR_ID_PREFIX, '') + '-color)')) {
				option.innerHTML = event.detail.item.querySelector(SELECTOR_CONTROL_HEAD).innerHTML;
			}
		});
	}

	/**
	 * Handle color updated to update select-color color values without refreshing page
	 */
	onColorUpdated(event: CustomEvent) {
		// Update updated color in select to show new color next to color-select option
		this.refs.colorOptions.forEach((option) => {
			if(option.value.replace(/var\(|\)/gi, '') === event.detail.id) {
				option.dataset.color = event.detail.value;
			}
		});
	}
}
