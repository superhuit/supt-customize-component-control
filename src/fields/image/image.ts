export const SELECTOR = 'image-field';
interface WPMediaLibrary {
	on: Function,
	open: Function,
	state: Function,
}

interface RefsType {
	button: HTMLElement,
	input: HTMLInputElement,
	input_width?: HTMLInputElement,
	input_height?: HTMLInputElement,
	image: HTMLImageElement,
	mediaLibrary: WPMediaLibrary,
}

interface AttachmentAttributesType {
	url: string,
	width: string,
	height: string,
}
interface AttachmentType {
	attributes: AttachmentAttributesType,
}
interface StateType {
	attachment?: AttachmentType,
}

interface PropsType {
	onChange: Function
}

export class ImageField {
	el: HTMLElement
	props: PropsType
	refs: RefsType
	state: StateType

	constructor(el:HTMLElement, props: PropsType ) {
		this.el = el;
		this.props = props;
		this.state = {};

		this.refs = {
			button: this.el.querySelector('button'),
			input: this.el.querySelector('input'),
			input_width: this.el.querySelector('input.input-width'),
			input_height: this.el.querySelector('input.input-height'),
			image: this.el.querySelector('img'),
			mediaLibrary: this.initMediaLibrary(),
		};

		if ( this.refs.input.value !== '' ) {
			this.updateDOM();
		}

		this.refs.button.addEventListener('click', this.onButtonClick.bind(this))
	}

	initMediaLibrary() {
		// @ts-ignore
		const mediaLibrary: WPMediaLibrary = wp.media({
			frame: 'select',
			title: 'Select image',
			multiple : false,
			library: {
				type: [ 'image' ]
			},
		})

		mediaLibrary.on('open', this.onMediaLibraryOpen.bind(this))
		mediaLibrary.on('close', this.onMediaLibraryClose.bind(this))

		return mediaLibrary;
	}

	updateDOM() {
		if(!this.refs.input.value) return;

		if(!this.refs.image) {
			this.refs.image = document.createElement('img');
			this.refs.button.appendChild(this.refs.image);
		}

		this.refs.image.src = this.refs.input.value;
		this.refs.button.classList.add("has-image");
	}

	onButtonClick() {
		this.refs.mediaLibrary.open()
	}

	// Select the appropiate image in the media manager
	onMediaLibraryOpen() {
		this.refs.mediaLibrary.state().get('selection').add( this.state?.attachment ? [ this.state.attachment ] : [] );
	}

	onMediaLibraryClose() {
		const selection =  this.refs.mediaLibrary.state().get('selection');
		if ( selection.length > 0 ) {
			const attachment = selection.models[0]
			this.state = { attachment }
		}
		else {
			this.state = {}
		}

		this.refs.input.value = this.state?.attachment?.attributes.url ?? '';

		// Set width and height inputs with image width/height if needed
		if(this.refs.input_width) this.refs.input_width.value = this.state.attachment?.attributes?.width;
		if(this.refs.input_height) this.refs.input_height.value = this.state.attachment?.attributes?.height;

		this.updateDOM();
		this.props.onChange();
	}
}
