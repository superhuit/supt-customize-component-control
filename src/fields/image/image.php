<?php

namespace SUPT\Customizer\Control\Fields\Image;

function render_field($id, $name, $attrs, $echo = true) {
	$value = $attrs['value'][$name] ?? null;
	$value_width = $attrs['value'][$name.'-width'] ?? null;
	$value_height = $attrs['value'][$name.'-height'] ?? null;

	$data = [
		'%label'    		=> $attrs['label'],
		'%id'       		=> "{$id}_{$name}",
		'%name'     		=> "{$name}",
		'%value_img' 		=> $value ? str_replace(['url(', ')'], '', $value) : ($attrs['default']),
		'%btnTitle' 		=> esc_html__('Select image', 'supt'),
		'%value_width'  => $value_width,
		'%value_height' => $value_height
	];

	$html = str_replace(
		array_keys($data),
		array_values($data),
		'<div class="supt-customize-component-control__field image-field">
			<label for="%id">%label</label>
			<button type="button" class="upload-button button-add-media image-field__button" id="%id"><span>%btnTitle</span></button>
			<input type="hidden" name="%name" value="%value_img" />
			<input type="hidden" name="%name-width" value="%value_width" class="input-width" />
			<input type="hidden" name="%name-height" value="%value_height" class="input-height" />
		</div>'
	);

	if ($echo) echo $html;
	return $html;
}
