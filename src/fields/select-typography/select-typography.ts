export const SELECTOR = 'select-typography-field';
const CUSTOM_TYPO_ID_PREFIX = "supt-customize-component-control-supt_settings_typography-";
const SELECTOR_CONTROL_HEAD = '.supt-customize-component-control__head-title';

interface RefsType {
	select: HTMLSelectElement,
	options: Array<HTMLOptionElement>
}

export class SelectTypoField {
	el
	refs: RefsType

	constructor(el: HTMLElement) {
		this.onCustomTypographyAdded = this.onCustomTypographyAdded.bind(this);
		this.onCustomTypographyDeleted = this.onCustomTypographyDeleted.bind(this);
		this.onCustomTypographyUpdated = this.onCustomTypographyUpdated.bind(this);

		this.el = el;

		this.refs = {
			select: this.el.querySelector("select"),
			options: Array.from(this.el.querySelectorAll("option"))
		}

		// event listeners
		document.addEventListener('custom-typography-added', this.onCustomTypographyAdded)
		document.addEventListener('custom-typography-deleted', this.onCustomTypographyDeleted)
		document.addEventListener('custom-typography-updated', this.onCustomTypographyUpdated)
	}

	onCustomTypographyAdded(event: CustomEvent) {
		event.preventDefault();

		// Add new option in select with new typography item added
		const newOption = document.createElement('option');
		newOption.setAttribute('value', event.detail.newItem.id.replace(CUSTOM_TYPO_ID_PREFIX, ''));
		newOption.innerHTML = event.detail.newItem.querySelector(SELECTOR_CONTROL_HEAD).innerHTML;

		this.refs.select.appendChild(newOption);
		this.refs.options.push(newOption);
	}

	onCustomTypographyDeleted(event: CustomEvent) {
		// Remove deleted item/option in select
		this.refs.options.forEach((option, index) => {
			if(option.value === event.detail.item.id.replace(CUSTOM_TYPO_ID_PREFIX, '')) {
				this.refs.select.removeChild(option);
				this.refs.options.splice(index, 1);
			}
		});
	}

	onCustomTypographyUpdated(event: CustomEvent) {
		// Remove deleted item/option in select
		this.refs.options.forEach((option, index) => {
			if(option.value === event.detail.item.id.replace(CUSTOM_TYPO_ID_PREFIX, '')) {
				option.innerHTML = event.detail.item.querySelector(SELECTOR_CONTROL_HEAD).innerHTML;
			}
		});
	}
}
