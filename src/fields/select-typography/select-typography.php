<?php

namespace SUPT\Customizer\Control\Fields\SelectTypography;

class Walker {

	function __construct($id, $name, $attrs) {
		$this->id = $id;
		$this->name = $name;
		$this->attrs = $attrs;
	}

	function get_id() {
		return "{$this->id}_{$this->name}";
	}

	function get_rendered() {
		return
			'<fieldset class="supt-customize-component-control__field select-typography-field">
				<legend class="select-typography-field__legend">'. $this->attrs['label'] .'</legend>
				<div class="select-typography-field__inner">
					'. $this->get_select_field() .'
					'. $this->get_alignment_field() .'
				</div>
			</fieldset>';
	}

	function get_select_field() {
		$default_options = $this->get_default_typography_options();
		$custom_options = $this->get_custom_typography_options();

		$value = $this->attrs['value'][$this->name] ?? $this->attrs['default'] ?? '';

		$options = [];
		foreach (array_merge($default_options, $custom_options) as $v => $n) {
			$options[] = sprintf(
				'<option value="%1$s"%3$s>%2$s</option>',
				$v,
				$n,
				( $v == $value ? ' selected' : '')
			);
		}

		$data = [
			'%id'      => $this->id . '-' . $this->name,
			'%name'		 => $this->name,
			'%value'   => $value,
			'%options' => implode("\n", $options),
		];


		return str_replace(
			array_keys($data),
			array_values($data),
			'<div class="select-typography-field__item">
				<select name="%name" id="%id" value="%value">%options</select>
			</div>'
		);
	}

	function get_default_typography_options() {
		return array_reduce(array_keys($this->attrs['default_typographies']), function($acc, $key) {
			$acc += [$key => $this->attrs['default_typographies'][$key]['label']];

			return $acc;
		}, []);
	}

	function get_custom_typography_options() {
		$options = [];
		$custom_typography = array_fill(1, 20, '');

		return array_reduce(array_keys($custom_typography), function($acc, $key) {
			$settings = json_decode(get_theme_mod(sprintf($this->attrs['typographies_setting_id'], '--t-custom-' . $key), "{}"), true);

			$option_name = $settings['--t-custom-' . $key . '-custom-name'] ?? "Custom typography " . $key;

			if (!empty($settings)) $acc += ['--t-custom-' . $key => $option_name];

			return $acc;
		}, []);
	}


	function get_alignment_field() {
		$items = [
			['%id' => "{$this->id}-{$this->name}_textAlign_reset",   '%label' => __('Clear', 'supt-ccc'),   '%name' => "{$this->name}-ta", "%value" => '',        '%icon' => 'removeformatting' ],
			['%id' => "{$this->id}-{$this->name}_textAlign_left",    '%label' => __('Left', 'supt-ccc'),    '%name' => "{$this->name}-ta", "%value" => 'left',    '%icon' => 'alignleft' ],
			['%id' => "{$this->id}-{$this->name}_textAlign_center",  '%label' => __('Center', 'supt-ccc'),  '%name' => "{$this->name}-ta", "%value" => 'center',  '%icon' => 'aligncenter' ],
			['%id' => "{$this->id}-{$this->name}_textAlign_right",   '%label' => __('Right', 'supt-ccc'),   '%name' => "{$this->name}-ta", "%value" => 'right',   '%icon' => 'alignright' ],
			['%id' => "{$this->id}-{$this->name}_textAlign_justify", '%label' => __('Justify', 'supt-ccc'), '%name' => "{$this->name}-ta", "%value" => 'justify', '%icon' => 'justify' ],
		];

		$value = $this->attrs['value'][$this->name.'-ta'] ?? '';

		$rendered_items = array_map(function($item) use ($value) {
			$item['%checked'] = ( (!empty($value) && $item['%value'] == $value) ? ' checked' : '' );
			return str_replace(
				array_keys($item),
				array_values($item),
				'<div>
					<input type="radio" id="%id" name="%name" value="%value"%checked />
					<label for="%id" class="dashicons dashicons-editor-%icon">%label</label>
				</div>'
			);
		}, $items);

		return sprintf(
			'<div class="select-typography-field__item --text-align">
				<span>Text Align</span>
				%s
			</div>',
			implode("\n", $rendered_items)
		);
	}
}

function render_field($id, $name, $attrs, $echo = true) {
	$walker = new Walker($id, $name, $attrs);
	$html = $walker->get_rendered();

	if ($echo) echo $html;
	return $html;
}
