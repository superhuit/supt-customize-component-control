<?php

namespace SUPT\Customizer\Control\Fields\Typography;

class Walker {

	function __construct($id, $name, $attrs, $label) {
		$this->id = $id;
		$this->name = $name;
		$this->attrs = $attrs;
		$this->label = $label;
	}

	function get_id() {
		return "{$this->id}_{$this->name}";
	}

	function get_rendered() {
		return
			'<fieldset class="supt-customize-component-control__field field-typography"'. (isset($this->attrs["is_custom"]) ? "data-supt-custom-typography" : "") .'>
				<legend class="field-typography__legend">'. $this->attrs['label'] .'</legend>'.
				(isset($this->attrs["is_custom"]) ? $this->get_custom_name_input() : '').
				'<div class="field-typography__inner">
					'. $this->get_family_field() .'
					'. $this->get_weight_field() .'
					'. $this->get_size_fields() .'
					'. $this->get_transform_field() .'
				</div>' .
				(isset($this->attrs["is_custom"]) ? '<button class="field-typography__delete-btn button button-link button-link-delete">Delete</button>' : '').
			'</fieldset>';
	}

	function get_family_field() {
		if ( !in_array('family', $this->attrs['supports']) ) return '';

		return $this->get_rendered_select(
			"{$this->get_id()}_fontFamily",
			"{$this->name}-ff",
			'--font-family',
			__('Font Family', 'supt-ccc'),
			[
				'var(--font-primary)'   => __( 'Primary', 'supt-ccc'),
				'var(--font-secondary)' => __( 'Secondary', 'supt-ccc'),
			],
			$this->attrs['value'][$this->name.'-ff'] ?? $this->attrs['default']['fontFamily'] ?? '',
		);
	}

	function get_weight_field() {
		if ( !in_array('weight', $this->attrs['supports']) ) return '';

		$weights = [
			'100' => __('Thin (Hairline)', 'supt-ccc'),
			'200' => __('Extra Light (Ultra Light)', 'supt-ccc'),
			'300' => __('Light', 'supt-ccc'),
			'400' => __('Normal (Regular)', 'supt-ccc'),
			'500' => __('Medium', 'supt-ccc'),
			'600' => __('Semi Bold (Demi Bold)', 'supt-ccc'),
			'700' => __('Bold', 'supt-ccc'),
			'800' => __('Extra Bold (Ultra Bold)', 'supt-ccc'),
			'900' => __('Black (Heavy)', 'supt-ccc'),
		];
		// TODO find how to only show weight available
		// 	$available = get_available_weights($this->name);
		// 	$weights = array_intersect_key($weights, $variants);

		return $this->get_rendered_select(
			"{$this->get_id()}__fontWeight",
			"{$this->name}-fw",
			'--font-weight',
			__('Font Weight', 'supt-ccc'),
			$weights,
			$this->attrs['value'][$this->name.'-fw'] ?? $this->attrs['default']['fontWeight'] ?? '',
		);
	}

	function get_size_fields() {
		if ( !in_array('size', $this->attrs['supports']) ) return '';

		$marginUnit = $this->attrs['default']['margin']['unit'] ?? 'em';
		$marginTopValue = $this->attrs['value'][$this->name.'-mt'] ?? null;
		$marginBottomValue = $this->attrs['value'][$this->name.'-mb'] ?? null;

		$data = [
			'%id'            => $this->get_id(),
			'%name'          => $this->name,

			'%label_sizemin_fz'       => __('Min font size', 'supt-ccc'),
			'%label_sizemax_fz'       => __('Max font size', 'supt-ccc'),
			'%label_sizemin_lh'       => __('Min line height', 'supt-ccc'),
			'%label_sizemax_lh'       => __('Max line height', 'supt-ccc'),
			'%label_sizemin_ls'       => __('Min letter spacing', 'supt-ccc'),
			'%label_sizemax_ls'       => __('Max letter spacing', 'supt-ccc'),
			'%label_margin_top'       => __('Margin top', 'supt-ccc'),
			'%label_margin_bottom'       => __('Margin bottom', 'supt-ccc'),

			'%fontSizeMin'   => $this->attrs['value'][$this->name.'-fz-min'] ?? $this->attrs['default']['fontSize']['min'] ?? '', // No unit because of the calc fluid mixin
			'%fontSizeMax'   => $this->attrs['value'][$this->name.'-fz-max'] ?? $this->attrs['default']['fontSize']['max'] ?? '', // No unit because of the calc fluid mixin
			'%lineHeightMin'   => $this->attrs['value'][$this->name.'-lh-min'] ?? $this->attrs['default']['lineHeight']['min'] ?? '', // No unit because of the calc fluid mixin
			'%lineHeightMax'   => $this->attrs['value'][$this->name.'-lh-max'] ?? $this->attrs['default']['lineHeight']['max'] ?? '', // No unit because of the calc fluid mixin
			'%letterSpacingMin'   => $this->attrs['value'][$this->name.'-ls-min'] ?? $this->attrs['default']['letterSpacing']['min'] ?? '', // No unit because of the calc fluid mixin
			'%letterSpacingMax'   => $this->attrs['value'][$this->name.'-ls-max'] ?? $this->attrs['default']['letterSpacing']['max'] ?? '', // No unit because of the calc fluid mixin
			'%marginTop'   => $marginTopValue ? str_replace($marginUnit, '', $marginTopValue) : ($this->attrs['default']['margin']['top'] ?? ''),
			'%marginBottom'   => $marginBottomValue ? str_replace($marginUnit, '', $marginBottomValue) : ($this->attrs['default']['margin']['bottom'] ?? ''),
			'%marginUnit'   => $marginUnit,
		];

		return str_replace(
			array_keys($data),
			array_values($data),
			'<div class="field-typography__item --font-size-min">
				<label for="%id_fontSize_min">%label_sizemin_fz</label>
				<div class="field-typography__item-row">
					<span>px</span>
					<input type="number" id="%id_fontSize_min" name="%name-fz-min" value="%fontSizeMin" min="0" />
				</div>
			</div>
			<div class="field-typography__item --font-size-max">
				<label for="%id_fontSize_max">%label_sizemax_fz</label>
				<div class="field-typography__item-row">
					<span>px</span>
					<input type="number" id="%id_fontSize_max" name="%name-fz-max" value="%fontSizeMax" min="0" />
				</div>
			</div>

			<div class="field-typography__item --line-height-min">
				<label for="%id_lineHeight_min">%label_sizemin_lh</label>
				<div class="field-typography__item-row">
					<span>px</span>
					<input type="number" id="%id_lineHeight_min" name="%name-lh-min" value="%lineHeightMin" min="0" />
				</div>
			</div>
			<div class="field-typography__item --line-height-max">
				<label for="%id_lineHeight_max">%label_sizemax_lh</label>
				<div class="field-typography__item-row">
					<span>px</span>
					<input type="number" id="%id_lineHeight_max" name="%name-lh-max" value="%lineHeightMax" min="0" />
				</div>
			</div>

			<div class="field-typography__item --letter-spacing-min">
				<label for="%id_letterSpacing_min">%label_sizemin_ls</label>
				<div class="field-typography__item-row">
					<span>px</span>
					<input type="number" id="%id_letterSpacing_min" name="%name-ls-min" value="%letterSpacingMin" step=".1" />
				</div>
			</div>
			<div class="field-typography__item --letter-spacing-max">
				<label for="%id_letterSpacing_max">%label_sizemax_ls</label>
				<div class="field-typography__item-row">
					<span>px</span>
					<input type="number" id="%id_letterSpacing_max" name="%name-ls-max" value="%letterSpacingMax" step=".1" />
				</div>
			</div>

			<div class="field-typography__item --margin-top">
				<label for="%id_margin_top">%label_margin_top</label>
				<div class="field-typography__item-row">
					<span>%marginUnit</span>
					<input type="number" id="%id_margin_top" name="%name-mt" value="%marginTop" step=".1" data-unit="%marginUnit" />
				</div>
			</div>
			<div class="field-typography__item --margin-bottom">
				<label for="%id_margin_bottom">%label_margin_bottom</label>
				<div class="field-typography__item-row">
					<span>%marginUnit</span>
					<input type="number" id="%id_margin_bottom" name="%name-mb" value="%marginBottom" step=".1" data-unit="%marginUnit" />
				</div>
			</div>
			',
		);
	}

	function get_transform_field() {
		if ( !in_array('transform', $this->attrs['supports']) ) return '';

		return $this->get_rendered_select(
			"{$this->get_id()}_textTransform",
			"{$this->name}-tt",
			'--text-transform',
			__('Text Transform', 'supt-ccc'),
			[
				'none'					 => __('None', 'supt-ccc'),
				'capitalize'     => __('Capitalize', 'supt-ccc'),
				'uppercase'      => __('Uppercase', 'supt-ccc'),
				'lowercase'      => __('Lowercase', 'supt-ccc'),
				'full-width'     => __('Full-width', 'supt-ccc'),
				'full-size-kana' => __('Full-size-kana', 'supt-ccc'),
			],
			$this->attrs['value'][$this->name.'-tt'] ?? $this->attrs['default']['textTransform'] ?? '',
		);
	}


	function get_rendered_select($id, $name, $cls, $label, $choices = [], $value = null) {

		$opts = [];
		foreach ($choices as $optValue => $optLabel) {
			$opts[] = sprintf(
				'<option value="%1$s"%3$s>%2$s</option>',
				$optValue,
				$optLabel,
				( $value == $optValue ? ' selected' : '')
			);
		}

		$select_data = [
			'%id'      => $id,
			'%name'    => $name,
			'%class'   => $cls,
			'%label'   => $label,
			'%options' => implode("\n", $opts),
		];


		return str_replace(
			array_keys($select_data),
			array_values($select_data),
			'<div class="field-typography__item %class">
				<label for="%id">%label</label>
				<select id="%id" name="%name">
					%options
				</select>
			</div>'
		);
	}

	function get_custom_name_input() {
		$data = [
			'%id' 	 => "{$this->get_id()}_custom-name",
			'%name'  => "{$this->name}-custom-name",
			'%label' => __('Typography custom name', 'supt-ccc'),
			'%value' => $this->attrs['value'][$this->name.'-custom-name'] ?? $this->label ?? '',
		];

		return str_replace(
			array_keys($data),
			array_values($data),
			'<div class="field-typography__item field-typography__custom-name --custom-name">
				<label for="%id" class="field-typography__custom-name__label">%label</label>
				<input type="text" id="%id" class="field-typography__custom-name__input" name="%name" value="%value" />
			</div>'
		);
	}
}

function render_field($id, $name, $attrs, $echo = true, $label) {
	$walker = new Walker($id, $name, $attrs, $label);
	$html = $walker->get_rendered();

	if ($echo) echo $html;
	return $html;
}


