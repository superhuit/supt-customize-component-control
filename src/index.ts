import "./style.css"
import 'vanilla-delegation'

import { CustomizerComponentControl, SELECTOR as COMPONENT_CONTROL_SELECTOR } from "./controls/component-control";
import { CustomizerCustomTypographyControl, CustomizerButtonAddTypoControl, SELECTOR as SELECTOR_CUSTOM_TYPO, SELECTOR_ADD_TYPO_BUTTON } from './controls/custom-typography/custom-typography';
import { CustomizerCustomColorControl, CustomizerButtonAddColorControl, SELECTOR as SELECTOR_CUSTOM_COLOR, SELECTOR_ADD_COLOR_BUTTON } from './controls/custom-color/custom-color';
import { CustomizerAddThemeButtonControl, SELECTOR_ADD_THEME_BUTTON } from './controls/custom-theme/add-theme-button';
import { CustomizerCustomThemeControl, SELECTOR as SELECTOR_CUSTOM_THEME } from './controls/custom-theme/custom-theme';

let controls: Array<CustomizerComponentControl>
let controlsCustomTypo: Array<CustomizerCustomTypographyControl>
let controlsCustomColor: Array<CustomizerCustomColorControl>
let controlsTypoButton: CustomizerButtonAddTypoControl
let controlsColorButton: CustomizerButtonAddColorControl

// @ts-ignore
wp.customize.bind('ready', function () {
	/**
	 * INIT CONTROLS
	 */

	// Controls (default)
	const elements = Array.from(document.querySelectorAll(`${COMPONENT_CONTROL_SELECTOR}:not(${SELECTOR_CUSTOM_TYPO}):not(${SELECTOR_CUSTOM_COLOR}):not(${SELECTOR_CUSTOM_THEME})`));
	controls = elements.map((el: HTMLElement) => new CustomizerComponentControl(el, false) );


	// Custom typography controls
	const customTypoElements = Array.from(document.querySelectorAll(SELECTOR_CUSTOM_TYPO));
	controlsCustomTypo = customTypoElements.map((el: HTMLElement) => new CustomizerCustomTypographyControl(el) );

	// handle add typography button
	const elementTypoButton:HTMLButtonElement = document.querySelector(SELECTOR_ADD_TYPO_BUTTON);
	controlsTypoButton = new CustomizerButtonAddTypoControl(elementTypoButton);


	// Custom color controls
	const customColorElements = Array.from(document.querySelectorAll(SELECTOR_CUSTOM_COLOR));
	controlsCustomColor = customColorElements.map((el: HTMLElement) => new CustomizerCustomColorControl(el) );

	// handle add color button
	const elementColorButton:HTMLButtonElement = document.querySelector(SELECTOR_ADD_COLOR_BUTTON);
	controlsColorButton = new CustomizerButtonAddColorControl(elementColorButton);


	// Custom theme controls
	const customThemeElements = Array.from(document.querySelectorAll(SELECTOR_CUSTOM_THEME));
	customThemeElements.map((el: HTMLElement) => new CustomizerCustomThemeControl(el));

	// handle add theme button
	const elementThemeButton:HTMLButtonElement = document.querySelector(SELECTOR_ADD_THEME_BUTTON);
	new CustomizerAddThemeButtonControl(elementThemeButton);
});

