const path = require('path');

const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
	stats: 'errors-only',
	entry: {
		'supt-customize-component-control': path.resolve(__dirname, 'src', 'index.ts'),
	},
	output: {
		path: path.resolve( __dirname, 'dist' ),
		filename: '[name].js',
	},
	resolve: {
		extensions: [ '.ts', '.js' ],
	},
	plugins: [new MiniCssExtractPlugin({ 	filename: '[name].css', })],
	module: {
		rules: [
			{
				test: /\.ts$/,
				use: 'ts-loader',
				exclude: /node_modules/,
			},
			{
				test: /.css$/,
				use: [
					{
						loader: MiniCssExtractPlugin.loader,
					},
					{ loader: 'css-loader', options: { sourceMap: true , importLoaders: 1 }, },
					{
						loader: 'postcss-loader',

					},
				]
			}
		]
	},
	devServer: {
		contentBase: './dist',
		quiet: true,
		host: 'localhost',
		hot: true,
	},
}
